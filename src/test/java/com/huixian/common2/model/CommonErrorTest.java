package com.huixian.common2.model;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Description:   .
 *
 * @author : polegek
 * @date : Created in 2019-10-21 19:36
 */
public class CommonErrorTest {

    @Test
    public void getLayerNum() {
        assertEquals(0, CommonError.PARAM_ERROR.getLayerNum());
        assertEquals(0, CommonError.DB_ERROR.getLayerNum());
        assertEquals(0, CommonError.RATE_LIMIT.getLayerNum());
    }

    @Test
    public void getCode() {
    }

    @Test
    public void getDesc() {
    }
}