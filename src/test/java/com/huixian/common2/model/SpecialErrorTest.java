package com.huixian.common2.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Description:   .
 *
 * @author : polegek
 * @date : Created in 2019-10-21 19:40
 */
public class SpecialErrorTest {

    @Test
    public void getLayerNum() {
        assertEquals(0, SpecialError.DEVICE_ERROR.getLayerNum());
        assertEquals(0, SpecialError.USER_LOGIN_STATUS_ERROR.getLayerNum());
        assertEquals(0, SpecialError.USER_LOGIN_STATUS_ERROR_NOTIFY.getLayerNum());
    }

    @Test
    public void getCode() {
    }

    @Test
    public void getDesc() {
    }

}