package com.huixian.common2.exception;

import com.huixian.common2.model.CommonError;
import com.huixian.common2.model.SpecialError;
import org.junit.Assert;
import org.junit.Test;

/**
 * Description:   .
 *
 * @author : polegek
 * @date : Created in 2019-10-21 15:43
 */
public class FrameworkExceptionTest {

    /**
     * 错误信息
     */
    private String errorMsg = "This is an error message";

    /**
     * Throwable 信息
     */
    private String throwableMsg = "Throwable value";

    @Test
    public void instance() {
        Assert.assertTrue(new FrameworkException() instanceof HuixianException);
    }

    @Test
    public void testException1() {
        try {
            throw new FrameworkException();
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getDesc(), e.getMessage());
        }
    }

    @Test
    public void testException2() {
        try {
            throw new FrameworkException(errorMsg);
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(errorMsg, e.getMessage());
        }
    }

    @Test
    public void testException3() {
        try {
            throw new FrameworkException(new Throwable(throwableMsg));
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getDesc(), e.getMessage());
        }
    }

    @Test
    public void testException4() {
        try {
            throw new FrameworkException(errorMsg, new Throwable(throwableMsg));
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.SYSTEM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(errorMsg, e.getMessage());
        }
    }

    @Test
    public void testException5() {
        try {
            throw new FrameworkException(SpecialError.USER_LOGIN_STATUS_ERROR);
        } catch (FrameworkException e) {
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getErrorCode());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getDesc(), e.getMessage());
        }
    }

    @Test
    public void testException6() {
        try {
            throw new FrameworkException(SpecialError.USER_LOGIN_STATUS_ERROR, errorMsg);
        } catch (FrameworkException e) {
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getErrorCode());
            Assert.assertEquals(errorMsg, e.getMessage());
        }
    }

    @Test
    public void testException7() {
        try {
            throw new FrameworkException(SpecialError.USER_LOGIN_STATUS_ERROR, new Throwable(throwableMsg));
        } catch (FrameworkException e) {
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getErrorCode());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getDesc(), e.getMessage());
        }
    }

    @Test
    public void testException8() {
        try {
            throw new FrameworkException(SpecialError.USER_LOGIN_STATUS_ERROR, errorMsg, new Throwable(throwableMsg));
        } catch (FrameworkException e) {
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(SpecialError.USER_LOGIN_STATUS_ERROR.getCode(), e.getErrorCode());
            Assert.assertEquals(errorMsg, e.getMessage());
        }
    }

    @Test
    public void testException9() {
        try {
            throw new FrameworkException(CommonError.PARAM_ERROR);
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.PARAM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.PARAM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(CommonError.PARAM_ERROR.getDesc(), e.getMessage());
        }
    }

    @Test
    public void testException10() {
        try {
            throw new FrameworkException(CommonError.PARAM_ERROR, errorMsg);
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.PARAM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.PARAM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(errorMsg, e.getMessage());
        }
    }

    @Test
    public void testException11() {
        try {
            throw new FrameworkException(CommonError.PARAM_ERROR, new Throwable(throwableMsg));
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.PARAM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.PARAM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(CommonError.PARAM_ERROR.getDesc(), e.getMessage());
        }
    }

    @Test
    public void testException12() {
        try {
            throw new FrameworkException(CommonError.PARAM_ERROR, errorMsg, new Throwable(throwableMsg));
        } catch (FrameworkException e) {
            Assert.assertEquals(CommonError.PARAM_ERROR.getCode(), e.getBaseCode());
            Assert.assertEquals(CommonError.PARAM_ERROR.getLayerNum(), e.getLayer());
            Assert.assertEquals(0, e.getErrorCode());
            Assert.assertEquals(errorMsg, e.getMessage());
        }
    }

}