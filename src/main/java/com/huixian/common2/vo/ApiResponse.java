package com.huixian.common2.vo;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * create at  2018/07/19 13:49
 *
 * @author yanggang
 */
@Data
@ToString
public class ApiResponse<T> implements Serializable {

    private static final long serialVersionUID = 2177140286855555365L;

    @ApiModelProperty("错误码")
    private Integer errorCode = 0;

    @ApiModelProperty("错误描述")
    private String errorStr;

    @ApiModelProperty("结果")
    private T results;

    @ApiModelProperty("token")
    private String token;

    public ApiResponse() {
    }

    public ApiResponse(T results) {
        this.results = results;
    }
}
