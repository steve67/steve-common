package com.huixian.common2.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * CreateBy KKYV 2019/1/9
 */
@Data
public class Pagination<T> implements Serializable {

    /**
     * 记录
     */
    private List<T> records;

    /**
     * 总条数
     */
    private long total;

    /**
     * 当前页大小
     */
    private long size;

    /**
     * 当前页
     */
    private long current;

    /**
     * 总页数
     */
    private long pages;

    /**
     * 是否有下一页
     */
    @Deprecated
    private Boolean isNext;

    /**
     * 是否有下一页
     */
    private Boolean hasNext;

    @Deprecated
    public Boolean getIsNext() {
        return current < pages;
    }

    public Boolean getHasNext() {
        return current < pages;
    }
}
