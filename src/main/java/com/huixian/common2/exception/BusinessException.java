package com.huixian.common2.exception;

import com.huixian.common2.model.IError;
import com.huixian.common2.model.SpecialError;
import java.io.Serializable;

/**
 * Huixian 业务异常
 *
 * @author : polegek
 * @date : Created in 2019-10-21 13:58
 */
public class BusinessException extends HuixianException implements Serializable {

    private static final long serialVersionUID = -2930857802117499649L;

    /**
     * 默认错误为系统级错误
     */
    public BusinessException() {
        super();
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     */
    public BusinessException(String message) {
        super(message);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param throwable 抛出内容
     */
    public BusinessException(Throwable throwable) {
        super(throwable);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     * @param throwable 抛出内容
     */
    public BusinessException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     */
    public BusinessException(SpecialError specialError) {
        super(specialError);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param message      错误信息
     */
    public BusinessException(SpecialError specialError, String message) {
        super(specialError, message);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param throwable    抛出内容
     */
    public BusinessException(SpecialError specialError, Throwable throwable) {
        super(specialError, throwable);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param message      错误信息
     * @param throwable    抛出内容
     */
    public BusinessException(SpecialError specialError, String message, Throwable throwable) {
        super(specialError, message, throwable);
    }

    /**
     * 通用错误
     *
     * @param iError 通用错误类型
     */
    public BusinessException(IError iError) {
        super(iError);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param message      错误信息
     */
    public BusinessException(IError iError, String message) {
        super(iError, message);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param throwable    抛出内容
     */
    public BusinessException(IError iError, Throwable throwable) {
        super(iError, throwable);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param message      错误信息
     * @param throwable    抛出内容
     */
    public BusinessException(IError iError, String message, Throwable throwable) {
        super(iError, message, throwable);
    }
}
