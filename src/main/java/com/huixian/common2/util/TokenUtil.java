package com.huixian.common2.util;

import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.vo.RequestContext;

/**
 * Description :   .
 *
 * @author : frm
 * @date : Created in 2018/11/21 3:53 PM
 */
public class TokenUtil {

    public static String getActiveToken() {
        String hxToken = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_TOKEN.name());
        String hxReturnToken = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_RETURN_TOKEN.name());
        if (hxReturnToken != null) {
            return hxReturnToken;
        } else {
            return hxToken;
        }
    }
}
