package com.huixian.common2.util;

import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.model.IError;
import com.huixian.common2.vo.ApiResponse;
import com.huixian.common2.vo.RequestContext;
import org.apache.commons.lang3.StringUtils;

/**
 * create at  2018/09/05 10:54
 *
 * @author yanggang
 */
public class ApiResponseUtil {

    public static ApiResponse errorResponse(IError iError) {
        return errorResponse(iError, iError.getDesc());
    }

    public static ApiResponse errorResponse(IError iError, String errorStr) {
        Integer errorCode = Integer
                .valueOf(StringUtils
                        .join(iError.getSystemNum(), iError.getLayerNum(), iError.getBaseCode()));
        return errorResponse(errorCode, errorStr);
    }

    public static ApiResponse errorResponse(Integer errorCode, String errorStr) {
        String token = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_RETURN_TOKEN.name());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setErrorCode(errorCode);
        apiResponse.setErrorStr(errorStr);
        apiResponse.setToken(token);
        return apiResponse;
    }
}
