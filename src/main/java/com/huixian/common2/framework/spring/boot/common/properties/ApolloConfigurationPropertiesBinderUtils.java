package com.huixian.common2.framework.spring.boot.common.properties;

/**
 * Created by pengfei.dong
 * Date 2019-10-10
 * Time 17:41
 */

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.spring.config.ConfigPropertySource;
import com.ctrip.framework.apollo.spring.config.ConfigPropertySourceFactory;
import com.ctrip.framework.apollo.spring.util.SpringInjector;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.StringUtils;

public class ApolloConfigurationPropertiesBinderUtils {
  private static final Logger LOGGER = LoggerFactory.getLogger(ApolloConfigurationPropertiesBinderUtils.class);

  public ApolloConfigurationPropertiesBinderUtils() {
  }

  public static <T> T bindFromNamespace(DefaultListableBeanFactory beanFactory, String namespace, T bean) {
    LOGGER.debug("从[{}]读取配置[{}]", namespace, bean.getClass().getName());
    Config config = StringUtils.isEmpty(namespace) ? ConfigService.getAppConfig() : ConfigService.getConfig(namespace);
    ConfigPropertySourceFactory configPropertySourceFactory = (ConfigPropertySourceFactory)SpringInjector.getInstance(ConfigPropertySourceFactory.class);
    ConfigPropertySource source = configPropertySourceFactory.getConfigPropertySource(namespace, config);
    Annotation annotation = AnnotationUtils.findAnnotation(bean.getClass(), ConfigurationProperties.class);
    ResolvableType type = ResolvableType.forClass(bean.getClass());
    Bindable<?> target = Bindable.of(type).withExistingValue(bean).withAnnotations(new Annotation[]{annotation});
    String env = System.getProperty("spring.profiles.active");
    if("loc".equals(env)|| StringUtils.isEmpty(env)){
      Resource resource = new ClassPathResource("application-loc.properties");
      try {
        Properties props = PropertiesLoaderUtils.loadProperties(resource);
        MapPropertySource propertySource = new MapPropertySource("zk", (Map)props);
        (new ConfigurationPropertiesBinder(beanFactory, propertySource)).bind(target);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }else{
      (new ConfigurationPropertiesBinder(beanFactory, source)).bind(target);
    }
    return bean;
  }
}
