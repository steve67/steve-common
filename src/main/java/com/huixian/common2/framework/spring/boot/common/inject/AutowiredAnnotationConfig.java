package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:38
 */

import java.io.Serializable;
import java.util.StringJoiner;

public class AutowiredAnnotationConfig implements Serializable {
  private String value;
  private boolean primary;

  public AutowiredAnnotationConfig() {
  }

  public AutowiredAnnotationConfig(String value) {
    this.value = value;
  }

  public AutowiredAnnotationConfig(String value, boolean primary) {
    this.value = value;
    this.primary = primary;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public boolean isPrimary() {
    return this.primary;
  }

  public void setPrimary(boolean primary) {
    this.primary = primary;
  }

  public String toString() {
    return (new StringJoiner(", ", "[", "]")).add("value='" + this.value + "'").add("primary=" + this.primary).toString();
  }
}
