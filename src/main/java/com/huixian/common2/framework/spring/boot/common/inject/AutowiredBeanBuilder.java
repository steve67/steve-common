package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:39
 */

public interface AutowiredBeanBuilder<A> {
  Object build(A var1, Class<?> var2);
}

