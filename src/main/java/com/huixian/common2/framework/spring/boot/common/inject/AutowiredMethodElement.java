package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:40
 */

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.InjectionMetadata.InjectedElement;
import org.springframework.util.ReflectionUtils;

class AutowiredMethodElement<A> extends InjectedElement {
  private final Method method;
  private final A autowired;
  private final AutowiredBeanBuilder<A> builder;

  AutowiredMethodElement(Method method, PropertyDescriptor pd, A autowired, AutowiredBeanBuilder<A> builder) {
    super(method, pd);
    this.method = method;
    this.autowired = autowired;
    this.builder = builder;
  }

  protected void inject(Object bean, String beanName, PropertyValues pvs) throws Throwable {
    Class<?> autowiredClass = this.pd.getPropertyType();
    Object autowiredBean = this.builder.build(this.autowired, autowiredClass);
    ReflectionUtils.makeAccessible(this.method);
    this.method.invoke(bean, autowiredBean);
  }
}
