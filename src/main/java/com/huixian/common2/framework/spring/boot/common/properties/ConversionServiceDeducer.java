package com.huixian.common2.framework.spring.boot.common.properties;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:42
 */

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.boot.convert.ApplicationConversionService;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.GenericConverter;

public class ConversionServiceDeducer {
  private final DefaultListableBeanFactory beanFactory;

  ConversionServiceDeducer(DefaultListableBeanFactory beanFactory) {
    this.beanFactory = beanFactory;
  }

  public ConversionService getConversionService() {
    try {
      return (ConversionService)this.beanFactory.getBean("conversionService", ConversionService.class);
    } catch (NoSuchBeanDefinitionException var2) {
      return ((ConversionServiceDeducer.Factory)this.beanFactory.createBean(ConversionServiceDeducer.Factory.class)).create();
    }
  }

  private static class Factory {
    private List<Converter<?, ?>> converters = Collections.emptyList();
    private List<GenericConverter> genericConverters = Collections.emptyList();

    private Factory() {
    }

    @Autowired(
        required = false
    )
    @ConfigurationPropertiesBinding
    public void setConverters(List<Converter<?, ?>> converters) {
      this.converters = converters;
    }

    @Autowired(
        required = false
    )
    @ConfigurationPropertiesBinding
    public void setGenericConverters(List<GenericConverter> converters) {
      this.genericConverters = converters;
    }

    public ConversionService create() {
      if (this.converters.isEmpty() && this.genericConverters.isEmpty()) {
        return ApplicationConversionService.getSharedInstance();
      } else {
        ApplicationConversionService conversionService = new ApplicationConversionService();
        Iterator var2 = this.converters.iterator();

        while(var2.hasNext()) {
          Converter<?, ?> converter = (Converter)var2.next();
          conversionService.addConverter(converter);
        }

        var2 = this.genericConverters.iterator();

        while(var2.hasNext()) {
          GenericConverter genericConverter = (GenericConverter)var2.next();
          conversionService.addConverter(genericConverter);
        }

        return conversionService;
      }
    }
  }
}

