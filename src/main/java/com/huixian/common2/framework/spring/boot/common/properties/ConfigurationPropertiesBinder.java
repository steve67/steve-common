package com.huixian.common2.framework.spring.boot.common.properties;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:41
 */

import com.ctrip.framework.apollo.spring.config.ConfigPropertySource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.BindHandler;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.bind.PropertySourcesPlaceholdersResolver;
import org.springframework.boot.context.properties.bind.handler.IgnoreErrorsBindHandler;
import org.springframework.boot.context.properties.bind.handler.IgnoreTopLevelConverterNotFoundBindHandler;
import org.springframework.boot.context.properties.bind.handler.NoUnboundElementsBindHandler;
import org.springframework.boot.context.properties.bind.validation.ValidationBindHandler;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.ConfigurationPropertySources;
import org.springframework.boot.context.properties.source.UnboundElementsSourceFilter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.PropertySources;
import org.springframework.util.Assert;
import org.springframework.validation.Validator;

public class ConfigurationPropertiesBinder {
  private final DefaultListableBeanFactory beanFactory;
  private final PropertySource propertySource;
  private volatile Binder binder;

  public ConfigurationPropertiesBinder(DefaultListableBeanFactory beanFactory, ConfigPropertySource propertySource) {
    this.beanFactory = beanFactory;
    this.propertySource = propertySource;
  }
  public ConfigurationPropertiesBinder(DefaultListableBeanFactory beanFactory, PropertySource appliedPropertySource) {
    this.beanFactory = beanFactory;
    this.propertySource = appliedPropertySource;
  }

  public void bind(Bindable<?> target) {
    ConfigurationProperties annotation = (ConfigurationProperties)target.getAnnotation(ConfigurationProperties.class);
    Assert.state(annotation != null, () -> {
      return "Missing @ConfigurationProperties on " + target;
    });
    List<Validator> validators = this.getValidators(target);
    BindHandler bindHandler = this.getBindHandler(annotation, validators);
    this.getBinder().bind(annotation.prefix(), target, bindHandler);
  }

  private List<Validator> getValidators(Bindable<?> target) {
    List<Validator> validators = new ArrayList(3);
    if (target.getValue() != null && target.getValue().get() instanceof Validator) {
      validators.add((Validator)target.getValue().get());
    }

    return validators;
  }

  private BindHandler getBindHandler(ConfigurationProperties annotation, List<Validator> validators) {
    BindHandler handler = new IgnoreTopLevelConverterNotFoundBindHandler();
    if (annotation.ignoreInvalidFields()) {
      handler = new IgnoreErrorsBindHandler((BindHandler)handler);
    }

    if (!annotation.ignoreUnknownFields()) {
      UnboundElementsSourceFilter filter = new UnboundElementsSourceFilter();
      handler = new NoUnboundElementsBindHandler((BindHandler)handler, filter);
    }

    if (!validators.isEmpty()) {
      handler = new ValidationBindHandler((BindHandler)handler, (Validator[])validators.toArray(new Validator[0]));
    }

    return (BindHandler)handler;
  }

  private Binder getBinder() {
    if (this.binder == null) {
      this.binder = new Binder(this.getConfigurationPropertySources(), this.getPropertySourcesPlaceholdersResolver(), this.getConversionService(), this.getPropertyEditorInitializer());
    }

    return this.binder;
  }

  private Iterable<ConfigurationPropertySource> getConfigurationPropertySources() {
    return ConfigurationPropertySources.from(this.propertySource);
  }

  private PropertySourcesPlaceholdersResolver getPropertySourcesPlaceholdersResolver() {
    return new PropertySourcesPlaceholdersResolver(Arrays.asList(this.propertySource));
  }

  private ConversionService getConversionService() {
    return (new ConversionServiceDeducer(this.beanFactory)).getConversionService();
  }

  private Consumer<PropertyEditorRegistry> getPropertyEditorInitializer() {
    DefaultListableBeanFactory var10000 = this.beanFactory;
    return var10000::copyRegisteredEditorsTo;
  }
}

