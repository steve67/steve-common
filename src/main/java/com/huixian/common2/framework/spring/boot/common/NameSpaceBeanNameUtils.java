package com.huixian.common2.framework.spring.boot.common;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:42
 */

public class NameSpaceBeanNameUtils {
  private static final String BEAN_NAME_SEPARATOR = "#";

  public NameSpaceBeanNameUtils() {
  }

  public static String build(Class<?> type, String namespace) {
    return type.getName() + "#" + namespace;
  }
}