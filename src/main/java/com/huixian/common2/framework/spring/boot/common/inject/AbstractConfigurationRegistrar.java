package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:38
 */

import java.util.LinkedList;
import java.util.List;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.StringUtils;

public abstract class AbstractConfigurationRegistrar implements ImportBeanDefinitionRegistrar {
  public AbstractConfigurationRegistrar() {
  }

  protected AutowiredAnnotationConfig[] parseAutowiredAnnotationConfigs(AnnotationMetadata metadata, String annotationName) {
    List<AutowiredAnnotationConfig> configs = new LinkedList();
    AnnotationAttributes attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(annotationName));
    String[] values = attributes.getStringArray("value");
    if (values != null && values.length > 0) {
      for(int i = 0; i < values.length; ++i) {
        if (!StringUtils.isEmpty(values[i])) {
          configs.add(new AutowiredAnnotationConfig(values[i], i == 0));
        }
      }
    }

    return (AutowiredAnnotationConfig[])configs.toArray(new AutowiredAnnotationConfig[0]);
  }
}