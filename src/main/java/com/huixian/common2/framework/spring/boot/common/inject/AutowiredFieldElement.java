package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:40
 */

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.InjectionMetadata.InjectedElement;
import org.springframework.util.ReflectionUtils;

class AutowiredFieldElement<A> extends InjectedElement {
  private final Field field;
  private final A autowired;
  private final AutowiredBeanBuilder<A> builder;

  AutowiredFieldElement(Field field, A autowired, AutowiredBeanBuilder builder) {
    super(field, (PropertyDescriptor)null);
    this.field = field;
    this.autowired = autowired;
    this.builder = builder;
  }

  protected void inject(Object bean, String beanName, PropertyValues pvs) throws Throwable {
    Class<?> autowiredType = this.field.getType();
    Object autowiredBean = this.builder.build(this.autowired, autowiredType);
    ReflectionUtils.makeAccessible(this.field);
    this.field.set(bean, autowiredBean);
  }
}
