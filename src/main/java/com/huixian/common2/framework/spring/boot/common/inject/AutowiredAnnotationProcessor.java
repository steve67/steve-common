package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:38
 */

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.annotation.InjectionMetadata.InjectedElement;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

public abstract class AutowiredAnnotationProcessor<B extends AutowiredBean, A extends Annotation> extends InstantiationAwareBeanPostProcessorAdapter implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware, DisposableBean, AutowiredBeanBuilder<A> {
  private static final Logger LOGGER = LoggerFactory.getLogger(AutowiredAnnotationProcessor.class);
  protected DefaultListableBeanFactory beanFactory;
  protected final ConcurrentMap<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap(256);
  protected final ConcurrentMap<String, B> autowiredBeansCache = new ConcurrentHashMap();
  protected final Class<A> annotationType;

  public AutowiredAnnotationProcessor(Class<A> annotationType) {
    this.annotationType = annotationType;
  }

  public AutowiredAnnotationProcessor(Class<A> annotationType, DefaultListableBeanFactory beanFactory, AutowiredAnnotationConfig... configs) {
    this.annotationType = annotationType;
    this.setBeanFactory(beanFactory);
    if (configs != null && configs.length > 0) {
      AutowiredAnnotationConfig[] var4 = configs;
      int var5 = configs.length;

      for(int var6 = 0; var6 < var5; ++var6) {
        AutowiredAnnotationConfig config = var4[var6];
        this.build((AutowiredAnnotationConfig)config, (Class)null);
      }
    }

  }

  public PropertyValues postProcessPropertyValues(PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) throws BeanCreationException {
    InjectionMetadata metadata = this.findAutowiredMetadata(beanName, bean.getClass(), pvs);

    try {
      metadata.inject(bean, beanName, pvs);
      return pvs;
    } catch (Throwable var7) {
      throw new BeanCreationException(beanName, "根据自定义注解注入bean失败", var7);
    }
  }

  private List<InjectedElement> findFieldAutowiredMetadata(Class<?> beanClass) {
    List<InjectedElement> elements = new LinkedList();
    ReflectionUtils.doWithFields(beanClass, (field) -> {
      A autowired = AnnotationUtils.getAnnotation(field, this.annotationType);
      if (autowired != null) {
        if (Modifier.isStatic(field.getModifiers())) {
          LOGGER.warn("@{} annotation is not supported on static fields: {}", this.annotationType, field);
          return;
        }

        elements.add(new AutowiredFieldElement(field, autowired, this));
      }

    });
    return elements;
  }

  private List<InjectedElement> findMethodAutowiredMetadata(Class<?> beanClass) {
    List<InjectedElement> elements = new LinkedList();
    ReflectionUtils.doWithMethods(beanClass, (method) -> {
      Method bridgedMethod = BridgeMethodResolver.findBridgedMethod(method);
      if (BridgeMethodResolver.isVisibilityBridgeMethodPair(method, bridgedMethod)) {
        A autowired = AnnotationUtils.findAnnotation(bridgedMethod, this.annotationType);
        if (autowired != null && method.equals(ClassUtils.getMostSpecificMethod(method, beanClass))) {
          if (Modifier.isStatic(method.getModifiers())) {
            LOGGER.warn("@{} annotation is not supported on static methods: {}", this.annotationType, method);
            return;
          }

          if (method.getParameterTypes().length == 0) {
            LOGGER.warn("@{}  annotation should only be used on methods with parameters: {}", this.annotationType, method);
          }

          PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, beanClass);
          elements.add(new AutowiredMethodElement(method, pd, autowired, this));
        }

      }
    });
    return elements;
  }

  private InjectionMetadata buildAutowiredMetadata(Class<?> beanClass) {
    List<InjectedElement> elements = new LinkedList();
    elements.addAll(this.findFieldAutowiredMetadata(beanClass));
    elements.addAll(this.findMethodAutowiredMetadata(beanClass));
    return new InjectionMetadata(beanClass, elements);
  }

  private InjectionMetadata findAutowiredMetadata(String beanName, Class<?> clazz, PropertyValues pvs) {
    String cacheKey = StringUtils.hasLength(beanName) ? beanName : clazz.getName();
    InjectionMetadata metadata = (InjectionMetadata)this.injectionMetadataCache.get(cacheKey);
    if (InjectionMetadata.needsRefresh(metadata, clazz)) {
      synchronized(this.injectionMetadataCache) {
        metadata = (InjectionMetadata)this.injectionMetadataCache.get(cacheKey);
        if (InjectionMetadata.needsRefresh(metadata, clazz)) {
          if (metadata != null) {
            metadata.clear(pvs);
          }

          try {
            metadata = this.buildAutowiredMetadata(clazz);
            this.injectionMetadataCache.put(cacheKey, metadata);
          } catch (NoClassDefFoundError var9) {
            throw new IllegalStateException("Failed to introspect bean class [" + clazz.getName() + "] for autowired metadata: could not find class that it depends on", var9);
          }
        }
      }
    }

    return metadata;
  }

  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = (DefaultListableBeanFactory)beanFactory;
  }

  public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
    if (beanType != null) {
      InjectionMetadata metadata = this.findAutowiredMetadata(beanName, beanType, (PropertyValues)null);
      metadata.checkConfigMembers(beanDefinition);
    }

  }

  public int getOrder() {
    return 2147483645;
  }

  public void destroy() {
    Iterator var1 = this.autowiredBeansCache.values().iterator();

    while(var1.hasNext()) {
      B bean = (B) var1.next();
      LOGGER.debug("Shutting Down : {}", bean);

      try {
        bean.destroy();
      } catch (Exception var4) {
        LOGGER.warn("Destroy Bean Failed !", var4);
      }
    }

    this.injectionMetadataCache.clear();
    this.autowiredBeansCache.clear();
  }

  public Object build(A autowired, Class<?> autowiredType) {
    AutowiredAnnotationConfig config = this.parseConfig(autowired);
    Assert.hasLength(config.getValue(), "请指定配置信息所在的Namespace");
    return this.build(config, autowiredType);
  }

  protected Object build(AutowiredAnnotationConfig config, Class<?> autowiredType) {
    B autowiredBean = (B) this.autowiredBeansCache.get(config.getValue());
    if (autowiredBean == null) {
      LOGGER.debug("读取Namespace=[{}]下的配置并创建AutowiredBean . . .", config.getValue());
      autowiredBean = this.create(config);
      this.autowiredBeansCache.putIfAbsent(config.getValue(), autowiredBean);
    }

    Object targetBean = null;
    if (autowiredType != null) {
      LOGGER.debug("自动注入:NameSpace=[{}] , Bean=[{}]", config.getValue(), autowiredType);
      targetBean = this.getBean(autowiredBean, autowiredType);
      if (targetBean == null) {
        throw new NoSuchBeanDefinitionException(autowiredType, "不支持的注入类型");
      }
    }

    return targetBean;
  }

  protected abstract AutowiredAnnotationConfig parseConfig(A var1);

  protected abstract B create(AutowiredAnnotationConfig var1);

  protected abstract Object getBean(B var1, Class<?> var2);
}

