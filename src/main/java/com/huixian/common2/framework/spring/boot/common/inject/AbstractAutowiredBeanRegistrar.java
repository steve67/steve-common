package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong
 * Date 2019-10-10
 * Time 17:35
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.util.Assert;

public abstract class AbstractAutowiredBeanRegistrar<T> {
  protected Logger logger = LoggerFactory.getLogger(this.getClass());
  protected DefaultListableBeanFactory beanFactory;
  protected AutowiredAnnotationConfig config;
  protected T autowiredBean;

  public AbstractAutowiredBeanRegistrar(AutowiredAnnotationConfig config, DefaultListableBeanFactory beanFactory) {
    this.beanFactory = beanFactory;
    this.config = config;
    Assert.hasLength(config.getValue(), "请指定配置所在的Namespace");
  }

  protected void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
    beanDefinition.setPrimary(this.config.isPrimary());
    this.beanFactory.registerBeanDefinition(beanName, beanDefinition);
    if (this.config.isPrimary()) {
      this.logger.info("根据配置将当前Bean暴露为Primary:{}", beanName);
    }

  }

  protected void registerSingleton(String beanName, Object bean) {
    this.beanFactory.registerSingleton(beanName, bean);
  }

  public T register() {
    if (this.beanFactory.containsBean(this.config.getValue())) {
      this.logger.debug("AutowiredBean已存在,直接返回:{}", this.config.getValue());
      this.autowiredBean = (T) this.beanFactory.getBean(this.config.getValue());
    } else {
      this.logger.info("AutowiredBean不存在,即刻创建:{}", this.config);
      this.autowiredBean = this.build();
      this.registerSingleton(this.config.getValue(), this.autowiredBean);
    }

    return this.autowiredBean;
  }

  protected abstract T build();
}

