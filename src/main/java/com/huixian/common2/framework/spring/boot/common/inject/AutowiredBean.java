package com.huixian.common2.framework.spring.boot.common.inject;

/**
 * Created by pengfei.dong Date 2019-10-10 Time 17:39
 */

import org.springframework.beans.factory.DisposableBean;

public abstract class AutowiredBean<T> implements DisposableBean {
  private String namespace;
  private T properties;

  public AutowiredBean(String namespace, T properties) {
    this.namespace = namespace;
    this.properties = properties;
  }

  public String getNamespace() {
    return this.namespace;
  }

  public T getProperties() {
    return this.properties;
  }

  public String toString() {
    return this.getClass().getName() + "#" + this.namespace;
  }
}
