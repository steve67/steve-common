package com.huixian.common2.framework.spring.boot.common.properties;

/**
 * Created by pengfei.dong
 * Date 2019-10-10
 * Time 17:42
 * 用具多租户隔离  隐式传参
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

public class TraceContext {
  private static final Logger LOGGER = LoggerFactory.getLogger(TraceContext.class);
  public static final String DUBBO_CONTEXT = "dubbo.context";
  public static final Long MAX_LENGTH = 2000L;
  public static final String FRAMEWORK_TRACE_ID = "framework_trace_id";
  private static final InheritableThreadLocal<TraceContext> CONTEXT_HOLDER = new InheritableThreadLocal<TraceContext>() {
    protected TraceContext initialValue() {
      return new TraceContext();
    }
  };
  private AtomicLong remainingLength;
  private Map<String, String> attachment;

  public TraceContext() {
    this.remainingLength = new AtomicLong(MAX_LENGTH);
    this.attachment = new ConcurrentHashMap();
  }

  public void put(String key, String value) throws Exception {
    if (key != null && value != null) {
      long length = this.lengthCheck(key, value);
      this.attachment.put(key, value);
      this.remainingLength.addAndGet(-length);
      if ("framework_trace_id".equals(key)) {
        MDC.put(key, value);
      }

    }
  }

  public void putAll(Map<String, String> map) throws Exception {
    if (!CollectionUtils.isEmpty(map)) {
      Iterator var2 = map.entrySet().iterator();

      while(var2.hasNext()) {
        Entry<String, String> entry = (Entry)var2.next();
        this.put((String)entry.getKey(), (String)entry.getValue());
      }

    }
  }

  public String get(String key) {
    return StringUtils.isEmpty(key) ? null : (String)this.attachment.get(key);
  }

  public Map<String, String> copyAttachment() {
    return new HashMap(this.attachment);
  }

  protected Long lengthCheck(String key, String value) throws Exception {
    long length;
    if (this.attachment.containsKey(key)) {
      length = (long)(value.length() - ((String)this.attachment.get(key)).length());
    } else {
      length = (long)(key.length() + value.length());
    }

    if (this.remainingLength.get() >= length) {
      return length;
    } else {
      throw new Exception(String.format("参数过长:%d>%d,key=%s,value=%s", MAX_LENGTH - this.remainingLength.get() + length, MAX_LENGTH, key, value));
    }
  }

  public static TraceContext getContext() {
    return (TraceContext)CONTEXT_HOLDER.get();
  }

  public static void remove() {
    CONTEXT_HOLDER.remove();
  }
}

